# Указываем, что мы хотим разворачивать окружение в AWS
terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}
# Создаем переменную с токеном. Берет токен из файла terraform.tfvars
variable "digitalocean_token" {}

# Указываем провайдер
provider "digitalocean" {
  token = var.digitalocean_token
}

resource "digitalocean_ssh_key" "default" {
  name       = "ssh key for DO"
  public_key = file("~/.ssh/id_rsa.pub")
}

# Ищем образ с последней версией Ubuntu
data "digitalocean_images" "ubuntu" {
  filter {
    key    = "distribution"
    values = ["Ubuntu"]
  }

  filter {
    key    = "regions"
    values = ["fra1"]
  }
}

resource "digitalocean_droplet" "nginx" {
  # Количество серверов
  count    = 2
  # С найденной Убунтой
  image    = data.digitalocean_images.ubuntu.images[0].slug
  name     = "nginx-${count.index}"
  region   = "fra1"
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
  tags     = ["nginx"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_loadbalancer" "web" {
  name        = "loadbalancer-1"
  region      = "fra1"
  droplet_ids = digitalocean_droplet.nginx.*.id


  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "digitalocean_droplet" "website" {
  image    = data.digitalocean_images.ubuntu.images[0].slug
  name     = "WebSite"
  region   = "fra1"
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
  tags     = ["backend", "react", "prod"]

  lifecycle {
    create_before_destroy = true
  }
}

output "nginx_ip" {
  description = "IP address of Nginx Servers"
  value       = digitalocean_droplet.nginx.*.ipv4_address
}

output "loadbalancer_url" {
  description = "URL For loadbalancer"
  value       = digitalocean_loadbalancer.web.ip
}

output "webserver_ip" {
  description = "IP address of WEB Server"
  value       = digitalocean_droplet.website.ipv4_address
}
